# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.6.0] - 2020-08-18
### Changed
- fixed an issue where import externals would reinstall gpm packages that didn't need to be installed
- importing a subtree no longer updates externals.json, which was causing builds to fail on the at the import externals step because the subtree code already exists in the host project... (if you have subtrees in your externals.json, please remove them manually)
- moved the subtree check for git status to subtree specific branch of import externals call, to allow importing a submodule with local changes present
- added the relative path from working directory to the composed-ci.json file, which can then be used to recreate all the paths when the project is opened from a different directory, leading to a much better experience when opening another developers build configuration

## [1.5.2] - 2020-06-30
### Changed
- import externals now uses gpm install with specific version number to avoid unneeded upgrades/updates
- improved import external unit test coverage


## [1.5.1] - 2020-06-25
### Changed
- fixed an issue with running vi tester and vi analyzer manually from configure build
- refactored CLI API VIs because useful messages were sometimes prevented from being written to the command line
- when open project lists bad vis, it will now list the qualified names for improved context
- Open project will only fail the build if there are more than 10 bad VIs (cyclic dependencies still won't cause an error)
- malleable VIs (.vim) are now ignored in the cyclic dependency check 
- Reactivated the cyclic dependency check, refactored the check algo

## [1.5.0] - 2020-06-16
### Changed
- Added options to the project recompile to give further control over what is compiled.  The CLI API has a new -c switch with the options sgxf, for source files, g packages, externals, and force compile. Default behavior is to force compile only the externals and g packages.
- Refactored Configure build to prompt with the new compile options when manually recompiling.
- Added new methods to the CLI API to set or remove custom VI search paths from the LabVIEW.ini file
- Added new methods to the CLI API to clear compiled object cache and app builder cache
- removed the control for the packages directory from configure build.vi it is hardcoded as gpm_packages by GPM, so the option here is not needed
- added new option to import externals using git subtrees as an alternative to git submodules

## [1.4.0] - 2020-02-11
### Changed
- Added GPM dependencies and license files as source to this package to remove any relinking on fresh download
- Created source distribution and NI package as new preferred way of package installation.  Won't require downloads for every project, and the VIs can be used to compile multiple LabVIEW versions and bitnesses.  
- Package installs to C:\Users\Public\Documents\LabVIEW Data\Composed-CI\ by default
- Import Externals no longer attempts to relink or recompile, as it was giving inconsistent results.  It is left to the developer to relink everything.  We have had best luck with Tools->Clear Compiled Object Cache.

## [1.3.1] - 2019-11-20
### Changed
- Moved gpm dependency location to remove requirement for recompile on package installation

## [1.3.0] - 2019-09-28
### Changed
- added Report Errors.vi to CLI api vis so the exact error number and message are sent to the command line output
- bunch of external dependency feature updates
- changed VI Tester and VI Analyzer step calls in Configure Build.vi to be by reference
- added dependencies vipc to the tools folder (in bitbucket source)

## [1.2.0] - 2019-08-30
### Added
- improved open project log message, now lists specific bad vis it found
- implemented 'import externals' as a method of adding private source code as a dependency via git, and resolving gpm dependencies and adding them to the parent project.  Specify a GIT URI to clone, or leave blank to read the externals.json file and import all.
- fixed an issue with the project recompile sorting order
- added cyclic dependency check to project recompile and open project steps
- switched config file format to JSON using MGI json library.  Existing binary configurations can still be read, but not created.

## [1.1.0] - 2019-04-25
### Added
- Added a new method called "Recompile Project" that will force a bottom-up recompile on all VIs in a project, including GPM dependencies.  This will fix any conflicts that may otherwise occur when opening the project and appear in the dependencies section.

## [1.0.1] - 2019-04-17
### Changed
- fixed an issue where dependency order sorting in the relink step did not work for some packages

## [1.0.0] - 2019-04-15
### Changed
- removed a breakpoint from manual build step that prevented builds from working
- updated default value of build target to be 'my computer'
- build step now creates zip files of build output
- build output is left in place, in case it is needed by subsequent build specs
- 'build specification names' no longer builds all if left blank.  Builds must be selected using the 'select build specs' button on the configure build UI

## [0.9.17] - 2019-02-28
### Changed
- manual API methods now generate usefult errors if input paramaters are bad
- improved test coverage for bad of missing configuration files
- improved test coverage for bad or missing working directory
- improved test coverage for VI tester steps, which must be run manually

## [0.9.16] - 2019-02-28
### Changed
- renamed composed-ci.ini to composed-ci.cfg
- added 'select build spec' button to UI
- added 'select tests' button to UI and reworked the VI tester operation to use a folder and whitelist/blacklist
### Removed
- removed pipeline build step

## [0.9.14] - 2019-02-26
### Changed
- removed dependency on ci-common-steps
- updated documentation and icon/avatar
- Open Project from CLI will return an error if <1 good VIs found or >0 bad VIs

## [0.9.12] - 2019-02-25
### Changed
- small refactor from VIA feedback
- added mass compile and relink steps to the CLI API
significant rework of the sweet user interface, including new buttons for mass compile and relinking

## [0.9.11] - 2019-02-19
### Changed
- updated composed-ci.ini
- removed configuration.lvlib
- removed dependency on @cs/configuration
- added open project step
- refactor due to new VIA configuration

## [0.1.0] - 2019-02-7
### Added
Initial release

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security