﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6056066</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">9868950</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16448250</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*-!!!*Q(C=\&gt;5`&lt;BJ"&amp;-@R8[Q5;&lt;G"R26?1:G'+X!"&amp;_]+F.G3+\QLU%=JE(S#JSBSYYID'"=5[5+_-\R&gt;7:&amp;C'NO+)M]SG0X.PQ_TSVLKZ:0U5;&gt;,:2A';@B&lt;;?XKLX-PD8VV4GF8_[RTLWGC];T;.4&lt;V9?/:JPG(;:7H:^6?PH(IH_M`7SZ_@&lt;W8^`*`F]M`AF@`(8R1?R"2ERJ5JZL;MK=E4`)E4`)E4`)A$`)A$`)A$X)H&gt;X)H&gt;X)H&gt;X)D.X)D.X)D.`*_E)N=Z#+(F#S?,*2-GES1&gt;);CZ#XR**\%EXAY6?**0)EH]31?OCDR**\%EXA3$]/5?"*0YEE]C9?JOC4\19YH]4#^!E`A#4S"*`#QJ!*0!!A7#S9/*I'BI$(Y%(A#4_$BIQ*0Y!E]A3@QU+T!%XA#4_!*0!TJOR*&gt;UQZS0%QDR_.Y()`D=4R-,=@D?"S0YX%],#@(YXA=B,/A-TE%/9/=$M[*YX%]`*(D=4S/R`%Y(JL[&amp;@+_-UX4$H)]BM@Q'"\$9XC91I&lt;(]"A?QW.YG&amp;;'R`!9(M.D?&amp;B+BM@Q'"Y$9CT+]D)G-Q9;H9T!]0$K&gt;YPVKR2&gt;9PW1[O:6X:3KGUVV%[FO$N6&amp;6VV-V563&lt;&lt;ZK5V7&lt;J&gt;I%V:&gt;4I659V3+KQ;WD$LTPK4PKFLKBLKEL[J)[J]\;U"@O?$A=N.`PN&gt;PNN.VON&gt;FMN&amp;[PN6KNN&amp;QO.:`0.:P.JM@!.=@U1$A`FY\5B__@&amp;X&gt;@@^X`O,XZ_@DN^P[98R:P^0`Z(XAW[EJ0VW#0@A0K#'A]!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="CLI API" Type="Folder">
		<Item Name="deprecated" Type="Folder">
			<Item Name="Call Mass Compile.vi" Type="VI" URL="../CLI API/Call Mass Compile.vi"/>
		</Item>
		<Item Name="SubVIs" Type="Folder">
			<Item Name="Parse Command Line Arguments.vi" Type="VI" URL="../CLI API/SubVIs/Parse Command Line Arguments.vi"/>
			<Item Name="Report Errors.vi" Type="VI" URL="../CLI API/SubVIs/Report Errors.vi"/>
			<Item Name="Working subdirectory checker.vi" Type="VI" URL="../CLI API/SubVIs/Working subdirectory checker.vi"/>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Call Clear Application Builder Cache.vi" Type="VI" URL="../CLI API/Call Clear Application Builder Cache.vi"/>
			<Item Name="Call Clear Compiled Object Cache.vi" Type="VI" URL="../CLI API/Call Clear Compiled Object Cache.vi"/>
			<Item Name="Call Remove Custom VI Search Path.vi" Type="VI" URL="../CLI API/Call Remove Custom VI Search Path.vi"/>
			<Item Name="Call Set Custom VI Search Path.vi" Type="VI" URL="../CLI API/Call Set Custom VI Search Path.vi"/>
		</Item>
		<Item Name="Call Build.vi" Type="VI" URL="../CLI API/Call Build.vi"/>
		<Item Name="Call Import Externals.vi" Type="VI" URL="../CLI API/Call Import Externals.vi"/>
		<Item Name="Call Open Project.vi" Type="VI" URL="../CLI API/Call Open Project.vi"/>
		<Item Name="Call Project Recompile.vi" Type="VI" URL="../CLI API/Call Project Recompile.vi"/>
		<Item Name="Call Relink.vi" Type="VI" URL="../CLI API/Call Relink.vi"/>
		<Item Name="Call VI Analyzer.vi" Type="VI" URL="../CLI API/Call VI Analyzer.vi"/>
		<Item Name="Call VI Tester.vi" Type="VI" URL="../CLI API/Call VI Tester.vi"/>
	</Item>
	<Item Name="Config File" Type="Folder">
		<Item Name="Read Configuration.vi" Type="VI" URL="../Config File/Read Configuration.vi"/>
		<Item Name="Write Configuration.vi" Type="VI" URL="../Config File/Write Configuration.vi"/>
	</Item>
	<Item Name="Externals" Type="Folder">
		<Item Name="Depricated" Type="Folder">
			<Item Name="Git Archive.vi" Type="VI" URL="../Git Dependency/Git Archive.vi"/>
		</Item>
		<Item Name="Externals.json" Type="Folder">
			<Item Name="Read ExternalDependency.vi" Type="VI" URL="../Git Dependency/Read ExternalDependency.vi"/>
			<Item Name="Write ExternalDependency.vi" Type="VI" URL="../Git Dependency/Write ExternalDependency.vi"/>
		</Item>
		<Item Name="gpackage.json" Type="Folder">
			<Item Name="Get Dependencies.vi" Type="VI" URL="../Git Dependency/Get Dependencies.vi"/>
			<Item Name="Set Dependency.vi" Type="VI" URL="../Git Dependency/Set Dependency.vi"/>
		</Item>
		<Item Name="GPM related" Type="Folder">
			<Item Name="Compare Dependencies.vi" Type="VI" URL="../Git Dependency/Compare Dependencies.vi"/>
			<Item Name="Creat Semver from Range.vi" Type="VI" URL="../Creat Semver from Range.vi"/>
			<Item Name="GPM install.vi" Type="VI" URL="../Git Dependency/GPM install.vi"/>
			<Item Name="GPM recompile.vi" Type="VI" URL="../Git Dependency/GPM recompile.vi"/>
			<Item Name="GPM relink.vi" Type="VI" URL="../Git Dependency/GPM relink.vi"/>
			<Item Name="GPM update.vi" Type="VI" URL="../Git Dependency/GPM update.vi"/>
		</Item>
		<Item Name="Git add subtree.vi" Type="VI" URL="../Git Dependency/Git add subtree.vi"/>
		<Item Name="Git Clone.vi" Type="VI" URL="../Git Dependency/Git Clone.vi"/>
		<Item Name="Git commit.vi" Type="VI" URL="../Git Dependency/Git commit.vi"/>
		<Item Name="Git pull subtree.vi" Type="VI" URL="../Git Dependency/Git pull subtree.vi"/>
		<Item Name="Git Rev.vi" Type="VI" URL="../Git Dependency/Git Rev.vi"/>
		<Item Name="Git rm subtree.vi" Type="VI" URL="../Git Dependency/Git rm subtree.vi"/>
		<Item Name="git status.vi" Type="VI" URL="../Git Dependency/git status.vi"/>
		<Item Name="import as.ctl" Type="VI" URL="../Git Dependency/import as.ctl"/>
		<Item Name="Parse subfolder name.vi" Type="VI" URL="../Git Dependency/Parse subfolder name.vi"/>
		<Item Name="Resolve GPM Dependencies.vi" Type="VI" URL="../Git Dependency/Resolve GPM Dependencies.vi"/>
		<Item Name="Sever Git.vi" Type="VI" URL="../Git Dependency/Sever Git.vi"/>
	</Item>
	<Item Name="Manual Steps" Type="Folder">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Item Name="SubVIs" Type="Folder">
			<Item Name="All VIs in Project.vi" Type="VI" URL="../VIA/All VIs in Project.vi"/>
			<Item Name="ZIP Build Files.vi" Type="VI" URL="../composed-ci/ZIP Build Files.vi"/>
			<Item Name="Apply test filters.vi" Type="VI" URL="../composed-ci/Apply test filters.vi"/>
			<Item Name="validate config file and working directory.vi" Type="VI" URL="../composed-ci/validate config file and working directory.vi"/>
			<Item Name="Validate configuration.vi" Type="VI" URL="../composed-ci/Validate configuration.vi"/>
			<Item Name="validate git string.vi" Type="VI" URL="../composed-ci/validate git string.vi"/>
			<Item Name="Changed VIs only.vi" Type="VI" URL="../VIA/Changed VIs only.vi"/>
		</Item>
		<Item Name="Manual Import Externals.vi" Type="VI" URL="../Manual API/Manual Import Externals.vi"/>
		<Item Name="Manual Build.vi" Type="VI" URL="../Manual API/Manual Build.vi"/>
		<Item Name="Manual VI Tester.vi" Type="VI" URL="../Manual API/Manual VI Tester.vi"/>
		<Item Name="Manual VIA.vi" Type="VI" URL="../Manual API/Manual VIA.vi"/>
		<Item Name="Manual Open Project.vi" Type="VI" URL="../Manual API/Manual Open Project.vi"/>
		<Item Name="Manual Mass Compile.vi" Type="VI" URL="../Manual API/Manual Mass Compile.vi"/>
		<Item Name="Manual Relink.vi" Type="VI" URL="../Manual API/Manual Relink.vi"/>
		<Item Name="Manual Compile Project.vi" Type="VI" URL="../Manual API/Manual Compile Project.vi"/>
	</Item>
	<Item Name="Recompile" Type="Folder">
		<Item Name="apply recompile options.vi" Type="VI" URL="../Recompile/apply recompile options.vi"/>
		<Item Name="Circular dependency check.vi" Type="VI" URL="../Recompile/Circular dependency check.vi"/>
		<Item Name="Compile All.vi" Type="VI" URL="../Relink/Compile All.vi"/>
		<Item Name="Improved Circular dependency check.vi" Type="VI" URL="../Recompile/Improved Circular dependency check.vi"/>
		<Item Name="Is external or g package.vi" Type="VI" URL="../Recompile/Is external or g package.vi"/>
		<Item Name="is malleable" Type="VI" URL="../Recompile/is malleable"/>
		<Item Name="Project Compile All.vi" Type="VI" URL="../Relink/Project Compile All.vi"/>
		<Item Name="Project Compile Externals.vi" Type="VI" URL="../Relink/Project Compile Externals.vi"/>
		<Item Name="Prompt for Recompile Options.vi" Type="VI" URL="../UI Support/Prompt for Recompile Options.vi"/>
		<Item Name="recompile options.ctl" Type="VI" URL="../Recompile/recompile options.ctl"/>
		<Item Name="Sort VIs.vi" Type="VI" URL="../Recompile/Sort VIs.vi"/>
		<Item Name="VI hierarchy cluster.ctl" Type="VI" URL="../Relink/VI hierarchy cluster.ctl"/>
	</Item>
	<Item Name="Relink" Type="Folder">
		<Item Name="Add G Package to Project.vi" Type="VI" URL="../Relink/Add G Package to Project.vi"/>
		<Item Name="Create temp project.vi" Type="VI" URL="../Relink/Create temp project.vi"/>
		<Item Name="Get G Package Dependencies.vi" Type="VI" URL="../Relink/Get G Package Dependencies.vi"/>
		<Item Name="List G packages.vi" Type="VI" URL="../Relink/List G packages.vi"/>
		<Item Name="Project Save All.vi" Type="VI" URL="../Relink/Project Save All.vi"/>
		<Item Name="Sort G Packages.vi" Type="VI" URL="../Relink/Sort G Packages.vi"/>
	</Item>
	<Item Name="Types" Type="Folder">
		<Item Name="build spec select.ctl" Type="VI" URL="../Types/build spec select.ctl"/>
		<Item Name="command line options.ctl" Type="VI" URL="../composed-ci/command line options.ctl"/>
		<Item Name="composed-ci configuration.ctl" Type="VI" URL="../Types/composed-ci configuration.ctl"/>
		<Item Name="g package dependencies.ctl" Type="VI" URL="../Types/g package dependencies.ctl"/>
		<Item Name="new button.ctl" Type="VI" URL="../Types/new button.ctl"/>
		<Item Name="open button.ctl" Type="VI" URL="../Types/open button.ctl"/>
		<Item Name="save button.ctl" Type="VI" URL="../Types/save button.ctl"/>
		<Item Name="test select options.ctl" Type="VI" URL="../Types/test select options.ctl"/>
		<Item Name="test select.ctl" Type="VI" URL="../Types/test select.ctl"/>
	</Item>
	<Item Name="UI Support" Type="Folder">
		<Item Name="Sandbox" Type="Folder">
			<Item Name="browse.ctl" Type="VI" URL="../UI Support/browse.ctl"/>
			<Item Name="Configure Build v2.vi" Type="VI" URL="../UI Support/Configure Build v2.vi"/>
			<Item Name="configure.ctl" Type="VI" URL="../UI Support/configure.ctl"/>
			<Item Name="configure2.ctl" Type="VI" URL="../UI Support/configure2.ctl"/>
		</Item>
		<Item Name="list build specs.vi" Type="VI" URL="../UI Support/list build specs.vi"/>
		<Item Name="list tests.vi" Type="VI" URL="../UI Support/list tests.vi"/>
		<Item Name="lv-ver.ctl" Type="VI" URL="../UI Support/lv-ver.ctl"/>
		<Item Name="Prompt for External URL.vi" Type="VI" URL="../UI Support/Prompt for External URL.vi"/>
		<Item Name="Select Build specs.vi" Type="VI" URL="../UI Support/Select Build specs.vi"/>
		<Item Name="Select tests.vi" Type="VI" URL="../UI Support/Select tests.vi"/>
	</Item>
	<Item Name="VI Search Path" Type="Folder">
		<Item Name="Remove Custom VI Search Path.vi" Type="VI" URL="../VI Search Path/Remove Custom VI Search Path.vi"/>
		<Item Name="Set Custom VI Search Path.vi" Type="VI" URL="../VI Search Path/Set Custom VI Search Path.vi"/>
	</Item>
	<Item Name="composed-ci.lvclass" Type="LVClass" URL="../composed-ci/composed-ci.lvclass"/>
	<Item Name="Configure Build.vi" Type="VI" URL="../Configure Build.vi"/>
</Library>
